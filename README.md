# Blackboard Connector
A connector for the Blackboard Learn REST API. Blackboard Learn is a learning management system.

Documentation: https://developer.blackboard.com/portal/displayApi

Swagger Specification: https://developer.blackboard.com/portal/docs/apis/learn-swagger.json
OAS3 Converted Specification: https://github.com/Enspire-Tech/openapi-connector-artifacts/blob/master/blackboard/custom-specification-blackboard.yaml

## Prerequisites

+ A Blackboard server using HTTPS
+ OAuth 2.0 setup with the "Client Credentials" authentication flow 

## Supported Operations

**All 248 endpoints are passing.**
## Connector Feedback

Feedback can be provided directly to Product Management in our [Product Feedback Forum](https://community.boomi.com/s/ideas) in the boomiverse.  When submitting an idea, please provide the full connector name in the title and a detailed description.

