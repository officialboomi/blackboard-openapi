// Copyright (c) 2022 Boomi, Inc.

package com.boomi.connector.blackboard;

import com.boomi.connector.api.OperationContext;
import com.boomi.connector.openapi.OpenAPIOperationConnection;
import com.boomi.util.LogUtil;
import org.apache.commons.lang3.StringUtils;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URL;
import java.time.Instant;
import java.util.Base64;
import java.util.concurrent.ConcurrentMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CustomOperationConnection extends OpenAPIOperationConnection {

    private static final Logger LOG = LogUtil.getLogger(CustomOperationConnection.class);
    private static final String BEARER_TOKEN = "bearer_token";
    private static final String EXPIRE_TIME = "expire_time";

    public CustomOperationConnection(OperationContext context) {
        super(context);
    }

    @Override
    public String getCustomAuthCredentials() {
        return "Bearer " + getBearerToken();
    }

    private String getBearerToken() {
        ConcurrentMap<Object, Object> cache = getContext().getConnectorCache();
        if (cache.containsKey(BEARER_TOKEN)) {
            long currentTime = Instant.now().getEpochSecond();
            String expireTime = String.valueOf(cache.getOrDefault(EXPIRE_TIME, "0"));
            if (StringUtils.isBlank(expireTime))
                    expireTime = "0";
            if (currentTime < Long.parseLong(expireTime)) {
                return String.valueOf(cache.get(BEARER_TOKEN));
            }
        }

        // Use oauth context to make an api call to the token endpoint
        String content = "grant_type=client_credentials";
        HttpsURLConnection connection = null;
        String bearerToken = "";
        String expireTime = "";
        Pattern accessTokenPattern = Pattern.compile(".*\"access_token\"\\s*:\\s*\"([^\"]+)\".*");//NOSONAR
        Pattern expiresInPattern = Pattern.compile(".*\"expires_in\"\\s*:\\s*(.+),.*");//NOSONAR
        try {
            String clientId =  getContext().getConnectionProperties().getProperty("clientId");
            String clientSecret = getContext().getConnectionProperties().getProperty("clientSecret");
            String tokenUrl = getContext().getConnectionProperties().getProperty("accessTokenUrl");

            URL url = new URL(tokenUrl);
            String authString = clientId + ":" + clientSecret;
            String encodedAuth = Base64.getEncoder().encodeToString(authString.getBytes());
            connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setRequestProperty("Authorization", "Basic " + encodedAuth);//NOSONAR Basic auth is required for this webservice
            try (PrintStream os = new PrintStream(connection.getOutputStream())) {
                os.print(content);
            }
            String line;
            String response;
            try (StringWriter out = new StringWriter(connection.getContentLength() > 0 ? connection.getContentLength() : 2048)) {
                try (BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                    while ((line = reader.readLine()) != null) {
                        out.append(line);
                    }
                }
                response = out.toString();
            }
            // Get bearer token
            Matcher matcher = accessTokenPattern.matcher(response);
            if (matcher.matches() && matcher.groupCount() > 0) {
                bearerToken = matcher.group(1);
            }

            // Get expire time
            matcher = expiresInPattern.matcher(response);
            if (matcher.matches() && matcher.groupCount() > 0) {
                int expiresIn = Integer.parseInt(matcher.group(1));
                Instant expireInstant = Instant.now().plusSeconds(expiresIn);
                expireTime = String.valueOf(expireInstant.getEpochSecond());
            }

            cache.remove(BEARER_TOKEN);
            cache.remove(EXPIRE_TIME);
            cache.put(BEARER_TOKEN, bearerToken);
            cache.put(EXPIRE_TIME, expireTime);

        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Unable to get OAuth2 access token", e);
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return bearerToken;
    }

}
